package com.sandbox.sbbc.controller;

import com.sandbox.sbbc.service.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

    @Autowired
    private MyService service;

    @RequestMapping("/value")
    public Double retrieveValue() {
        return service.calculateValue();
    }
}
