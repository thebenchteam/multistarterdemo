package com.sandbox.sbbc.service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Component;

@Component
@EnableCaching
public class MyService {

    /*
        We're relying on Spring Boot to auto configure a cache implementation
        Not suitable for production - you should configure one yourself

        We can tell the method is invoked if it prints out "invoked"
        If the output is returned and the output not printed, it came from cache
     */

    @Cacheable("myValue")
    public Double calculateValue() {
        System.out.println("invoked");
        return 3.14159;
    }
}
